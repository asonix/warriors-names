use anyhow::Result;
use rand::{seq::SliceRandom, Rng};
use std::path::Path;

#[derive(serde::Deserialize)]
pub(crate) struct Config {
    prefix: Vec<String>,
    suffix: Vec<String>,
}

impl Config {
    pub(crate) fn gen(&self, rng: &mut impl Rng) -> String {
        let prefix = self.prefix.choose(rng).map(|s| s.as_str()).unwrap();

        let suffixes = self
            .suffix
            .iter()
            .filter(|s| *s != prefix)
            .collect::<Vec<_>>();

        let suffix = suffixes.choose(rng).unwrap().to_string();

        let (first, rest) = prefix.split_at(1);

        first.to_uppercase() + rest + &suffix
    }
}

pub(crate) async fn config(path: impl AsRef<Path>) -> Result<Config> {
    let bytes = tokio::fs::read(path).await?;
    let config: Config = serde_json::from_slice(&bytes)?;

    Ok(config)
}
