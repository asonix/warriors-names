{ lib
, nixosTests
, rustPlatform
}:

rustPlatform.buildRustPackage {
  pname = "warriors-names";
  version = "0.2.3";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) warriors-names; };

  meta = with lib; {
    description = "A simple image hosting service";
    homepage = "https://git.asonix.dog/asonix/warriors-names";
    license = with licenses; [ agpl3Plus ];
  };
}
